module.exports = app => {
  app.get('/', (req, res) => {
    res.status(200).send({
      api: 'node-starter',
      v: '1.0.0'
    });
  });
};
