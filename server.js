const express = require('express');

const app = express();

// morgan
require('./config/morgan')(app);

// routes
require('./routes/rootRoutes')(app);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server started on PORT ${PORT}`);
});
