module.exports = app => {
  var fs = require('fs');
  var morgan = require('morgan');
  var path = require('path');
  var rfs = require('rotating-file-stream');

  var logDirectory = path.join(process.env.PWD, 'log');

  // ensure log directory exists
  fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

  // create a rotating write stream
  var accessLogStream = rfs('access.log', {
    interval: '1d', // rotate daily
    compress: 'gzip',
    path: logDirectory
  });

  // setup the logger
  app.use(morgan('combined', { stream: accessLogStream }));
};
